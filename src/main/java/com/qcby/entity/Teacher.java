package com.qcby.entity;

public class Teacher {
    private Integer id;

    private Integer tno;

    private String pwd;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTno() {
        return tno;
    }

    public void setTno(Integer tno) {
        this.tno = tno;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

	@Override
	public String toString() {
		return "Teacher [id=" + id + ", tno=" + tno + ", pwd=" + pwd + "]";
	}
    
}