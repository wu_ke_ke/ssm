package com.qcby.entity;

public class Student {
    private Long id;

    private Integer sno;

    private String pwd;

    private Integer leaveflag;

    private String teacher;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSno() {
        return sno;
    }

    public void setSno(Integer sno) {
        this.sno = sno;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    public Integer getLeaveflag() {
        return leaveflag;
    }

    public void setLeaveflag(Integer leaveflag) {
        this.leaveflag = leaveflag;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher == null ? null : teacher.trim();
    }

	@Override
	public String toString() {
		return "Student [id=" + id + ", sno=" + sno + ", pwd=" + pwd + ", leaveflag=" + leaveflag + ", teacher="
				+ teacher + "]";
	}
    
}