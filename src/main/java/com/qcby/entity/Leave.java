package com.qcby.entity;

public class Leave {
    private Integer id;

    private Integer sno;

    private Integer tno;

    private Integer leavestate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSno() {
        return sno;
    }

    public void setSno(Integer sno) {
        this.sno = sno;
    }

    public Integer getTno() {
        return tno;
    }

    public void setTno(Integer tno) {
        this.tno = tno;
    }

    public Integer getLeavestate() {
        return leavestate;
    }

    public void setLeavestate(Integer leavestate) {
        this.leavestate = leavestate;
    }

	@Override
	public String toString() {
		return "Leave [id=" + id + ", sno=" + sno + ", tno=" + tno + ", leavestate=" + leavestate + "]";
	}
    
}