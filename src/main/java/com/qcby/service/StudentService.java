package com.qcby.service;

import com.qcby.entity.Student;

public interface StudentService {
	//注册添加学生
	int add(Student student);
	
	//登录查找学生	
	String select(Integer id);
	
	//修改学生请假状态
	int update(Student student);
	
	//删除学生信息
	int delete(Integer id);
	
}
