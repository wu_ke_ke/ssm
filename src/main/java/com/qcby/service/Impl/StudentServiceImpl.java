package com.qcby.service.Impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qcby.dao.StudentMapper;
import com.qcby.entity.Student;
import com.qcby.service.StudentService;
@Service("studentService")
public class StudentServiceImpl  implements StudentService{
	@Autowired
    private StudentMapper studentMapper;
	
	@Override   //注册
	public int add(Student student) {
		// TODO 自动生成的方法存根
		return studentMapper.insert(student); 
	}

	@Override   //登录
	public String select(Integer sno) {
		// TODO 自动生成的方法存根
		return studentMapper.selectBySno(sno);
	}

	@Override
	public int update(Student student) {
		// TODO 自动生成的方法存根
		return studentMapper.updateByPrimaryKey(student);
	}

	@Override
	public int delete(Integer id) {
		// TODO 自动生成的方法存根
		return studentMapper.deleteByPrimaryKey(id);
	}

	

}
