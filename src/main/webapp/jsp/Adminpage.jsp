<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员主页界面</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>
<body>
<h2>管理员主页</h2>
<!-- 
	<div>
		<input type="button" value="显示学生信息" onclick="student();">
	</div> -->
	<!-- <div id ="student" style="display:none"> -->
<hr/>


	<form action="<%=request.getContextPath() %>/admin/showStudent" method="post">
		<input type="submit" value="显示学生信息">
			<table class="table">
				<thead>
					<tr>
						<td>ID</td>  <td>Sno</td>  <td>Pwd</td>   <td>leaveFlag</td>  <td>Teacher</td>
					</tr>
				</thead>
				<c:forEach items="${data }" var="data_student">
					<tr>
						<td>${data_student.id }</td>
						<td>${data_student.sno }</td>
						<td>${data_student.pwd }</td>
						<td>${data_student.leaveflag }</td>
						<td>${data_student.teacher }</td>
					</tr>
				</c:forEach>
			</table>	
	</form>


	<div>
		<h4>添加学生信息</h4>
		<form action="<%=request.getContextPath() %>/admin/addStudent" method="post">
			sno:       <input type="text" name="sno" />          
			pwd:       <input type="text" name="pwd" />      	 
			leaveflag  <input type="text" name="leaveflag" />  
			teacher:   <input type="text" name="teacher" />   	 	
			<input type="submit" value="添加">    <br/>	
		</form>
	</div>
	<div>
		<h4>删除学生信息</h4>
		<form action="<%=request.getContextPath() %>/admin/deleteStudent" method="post">
			sno:       <input type="text" name="sno" />          	 	 	
			<input type="submit" value="删除">     <br/>	
		</form>
	</div>
	<div>
		<h4>修改学生信息</h4>
		<form action="<%=request.getContextPath() %>/admin/updataStudent" method="post">
			id:       <input type="text" name="id" /> 
			sno:       <input type="text" name="sno" />          
			pwd:       <input type="text" name="pwd" />      	 
			leaveflag  <input type="text" name="leaveflag" />  
			teacher:   <input type="text" name="teacher" />   	 	
			<input type="submit" value="修改"><br/>	
		</form>
	</div>



<hr/>


<form action="<%=request.getContextPath() %>/admin/showTeacher" method="post">
	<input type="submit" value="显示教师信息">
	<table class="table">
		<thead>
			<tr>
				<td>ID</td>  <td>Tno</td>  <td>Pwd</td>   
			</tr>
		</thead>
		<c:forEach items="${data2 }" var="data2_teacher">
			<tr>
				<td>${data2_teacher.id }</td>
				<td>${data2_teacher.tno }</td>
				<td>${data2_teacher.pwd }</td>
			</tr>
		</c:forEach>
	</table>
</form>
<div>
	<h4>添加教师信息</h4>
	<form action="<%=request.getContextPath() %>/admin/addTeacher" method="post">
		tno:       <input type="text" name="tno" />          
		pwd:       <input type="text" name="pwd" />      	  	
		<input type="submit" value="添加">    <br/>	
	</form>
</div>
<div>
	<h4>删除教师信息</h4>
	<form action="<%=request.getContextPath() %>/admin/deleteTeacher" method="post">
		tno:       <input type="text" name="tno" />          	 	 	
		<input type="submit" value="删除">     <br/>	
	</form>
</div>
<div>
	<h4>修改教师信息</h4>
	<form action="<%=request.getContextPath() %>/admin/updataTeacher" method="post">
		id:       <input type="text" name="id" /> 
		tno:       <input type="text" name="tno" />          
		pwd:       <input type="text" name="pwd" />      	 	 	
		<input type="submit" value="修改"><br/>	
	</form>
</div>


</body>
	

<script type= "text/javascript">
	
/* 	var student = function(){
		location.href="${pageContext.request.contextPath}/admin/showStudent"; 
	} */
	//学生添加
	var msg = "${msg}";
	if(msg=="添加成功"){
		alert("添加成功！");			  	
	}else if(msg=="添加失败"){
		alert("添加失败，请修改信息！");
	}else if(msg=="已经存在"){
		alert("添加失败，信息已经存在！");
	}else if(msg=="不能为空"){
		alert("添加失败，信息不能存在为空值！");
	}
	//学生删除
	var msg2 = "${msg2}";
	if(msg2 =="删除成功"){		
		alert("删除成功！");			  	
	}else if(msg2 =="删除失败"){
		alert("删除失败！");
	}
	else if(msg2 =="不存在"){
		alert("删除失败，信息不存在！");		
	}else if(msg2 =="不能为空"){		
		alert("删除失败，信息不能存在为空值！");
		
	}
	//学生修改
	var msg3 = "${msg3}";
	if(msg3 =="修改成功"){		
		alert("修改成功！");				
	}else if(msg3 =="修改失败"){		
		alert("修改失败，请修改信息！");		
	}else if(msg3 =="sno不存在"){		
		alert("修改失败，sno信息不存在！");		
	}else if(msg3 == "不能为空"){	
		alert("修改失败，信息不能存在为空值！");
	}else if(msg3 == "修改重复"){
		alert("修改失败，要修改的sno已经存在!");
	}else if(msg3 =="id不存在"){		
		alert("修改失败，id信息不存在！");		
	}
	
	
	
	
	//教师添加
	var msg1 = "${msg1}";
	if(msg1 =="添加成功"){
		alert("添加成功！");			
	}else if(msg1 =="添加失败"){
		alert("添加失败，请修改信息！");
	}else if(msg1 == "不能为空"){
		alert("添加失败，Tno 或者 pwd 不能为空！");
	}else if(msg1 =="已经存在"){
		alert("添加失败，信息已经存在！");
	}
	
	//教师删除
	var msg4 = "${msg4}";
	if(msg4=="删除成功"){		
		alert("删除成功！");			  	
	}
	else if(msg4=="删除失败"){
		alert("删除失败！");
	}
	else if(msg4 =="不存在"){		
		alert("删除失败，信息不存在！");		
	}else if(msg4 =="不能为空"){	
		alert("删除失败，信息不能存在为空值！");		
	}
	
	//教师修改
	var msg5 = "${msg5}";
	if(msg5 =="修改成功"){	
		alert("修改成功！");				
	}else if(msg5 =="修改失败"){		
		alert("修改失败，请修改信息！");		
	}else if(msg5 =="不存在"){		
		alert("修改失败，id信息不存在！");		
	}else if(msg5 == "不能为空"){		
		alert("修改失败，信息不能存在为空值！");
	}else if(msg5 == "修改重复"){
		alert("修改失败，要修改的tno已经存在");
	}
	
</script>
</html>