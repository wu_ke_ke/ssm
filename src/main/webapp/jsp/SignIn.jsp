<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学生登录入口</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>

<body>
	<form action="<%=request.getContextPath() %>/login/select" method="post">
	<h4>学生登录入口</h4>
		sno: <input type="text" name="sno" />          	<br/>
		pwd: <input type="text" name="currpwd" />       <br/>
		<input type="submit" value="登录">
	</form>
	<div>  <input type="button" onclick="ff();" value="注册">   </div>
</body>
<script type= "text/javascript">
		var msg = "${msg}";
		if(msg=="登录成功"){
			alert("登录成功，即将跳转！");			
		    $(function () {
		       location.href="${pageContext.request.contextPath}/jsp/homepage.jsp";
		    });
		}else if(msg=="登录失败"){
			alert("登录失败，请核对登录信息是否有误！");
		}
		var ff=function(){
			location.href="${pageContext.request.contextPath}/jsp/login.jsp"; 
		}
</script>
</html>