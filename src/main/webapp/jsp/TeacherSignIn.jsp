<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>教师登录入口</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>

<body>
	<form action="<%=request.getContextPath() %>/teacherLogin/select" method="post">
	<h4>教师登录入口</h4>
		tno: <input type="text" name="sno" />          	<br/>
		pwd: <input type="text" name="currpwd" />       <br/>
		<input type="submit" value="登录">
	</form>
	<div>  <input type="button" onclick="ff();" value="注册">   </div>
</body>
<script type= "text/javascript">
		var msg = "${msg}";
		if(msg=="登录成功"){
			alert("登录成功，即将跳转！");			
		    $(function () {
		       location.href="${pageContext.request.contextPath}/jsp/teacherHome.jsp"; 
		    });
		}else if(msg=="登录失败"){
			alert("登录失败，请核对登录信息是否有误！");
		}
		else if(msg=="不存在"){
			alert("登录失败，tno不存在，请重新输入！");
		}
		else if(msg=="不能为空"){
			alert("登录失败，tno或pwd不能为空，请重新输入！");
		}
		var ff=function(){
			location.href="${pageContext.request.contextPath}/jsp/teacherRegister.jsp"; 
		}
	
</script>
</html>