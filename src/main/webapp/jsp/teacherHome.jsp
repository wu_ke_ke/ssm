<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>教师批准页面</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>
<body>
<h4>学生请假申请表<h4>
	<form action="<%=request.getContextPath() %>/leave/showLeave" method="get">
		<input type="submit" value="显示学生请假信息">
			<table class="table">
				<thead>
					<tr>
						<td>ID</td>  <td>Sno</td>  <td>Tno</td>   <td>leavestate</td> 
					</tr>
				</thead>
				<c:forEach items="${data }" var="data_leave">
					<tr>
						<td>${data_leave.id }</td>
						<td>${data_leave.sno }</td>
						<td>${data_leave.tno }</td>
						<td>${data_leave.leavestate }</td>
						<td>
							<form action="<%=request.getContextPath() %>/leave/updataLeave" method="get">
								<input type="hidden" name="sno" value="${data_leave.sno}"/>								
								<input type="submit" value="批准请假" />
							</form>
						</td>
					</tr>
				</c:forEach>
			</table>	
	</form>
	
	
	
	
	<h4>学生销假表<h4>
	<form action="<%=request.getContextPath() %>/leave/showLeave2" method="get">
		<input type="submit" value="显示学生销假信息">
			<table class="table">
				<thead>
					<tr>
						<td>ID</td>  <td>Sno</td>  <td>tno</td>   <td>leavestate</td> 
					</tr>
				</thead>
				<c:forEach items="${data2 }" var="data_leave2">
					<tr>
						<td>${data_leave2.id }</td>
						<td>${data_leave2.sno }</td>
						<td>${data_leave2.tno }</td>
						<td>${data_leave2.leavestate }</td>
						<td>
							<form action="<%=request.getContextPath() %>/leave/delLeave?sno=${data_leave2.sno}" method="get">								
								<input type="submit" value="批准销假" />
							</form>
						</td>
					</tr>
				</c:forEach>
			</table>	
	</form>
</body>
<script type= "text/javascript">
	
/* 	var student = function(){
		location.href="${pageContext.request.contextPath}/admin/showStudent"; 
	} */
	//教师
	var msg = "${msg}";
	if(msg=="批准成功"){
		alert("批准成功！");			  	
	}else if(msg=="批准成功"){
		alert("批准成功，请修改信息！");
	}
	
	var msg1 = "${msg}";
	if(msg1 =="销假成功"){
		alert("销假成功！");			  	
	}else if(msg1 =="销假失败"){
		alert("销假失败，请修改信息！");
	}
	
</script>

</html>